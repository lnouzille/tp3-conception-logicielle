import sqlite3


class SQLRequestsLogger:

    def init(self):
        connection = sqlite3.connect('db')
        cursor = connection.cursor()
        requete = "CREATE TABLE IF NOT EXISTS requete(requete_id INTEGER PRIMARY KEY AUTOINCREMENT, label text, date_text text)"
        cursor.execute(requete)
        connection.commit()

    def add_request(self, label):
        connection = sqlite3.connect('db')
        cursor = connection.cursor()
        requete = f"INSERT INTO requete(label,date_text) VALUES('{label}', datetime('now', 'localtime'))"
        cursor.execute(requete)
        connection.commit()

    def get_all_requests(self):
        connection = sqlite3.connect('db')
        cursor = connection.cursor()
        requete = "SELECT * from requete"
        res = cursor.execute(requete)
        data = res.fetchall()
        connection.commit()
        return data


if __name__ == "__main__":
    logger = SQLRequestsLogger()
    logger.init()
    logger.add_request("confiture")
    req = logger.get_all_requests()
    print(req)
